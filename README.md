# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

iTrust - Interconnect infrastructure
This connects Unity with Epanet using python as the middleman

### How do I get set up? ###

Ensure Epanettools lib is installed in your python  
Firstly, run the EPANET_python_publisher.py  
Next, open up Unity  
Finally, Run the prototype 2 scene  

You should be able to see the changes in water level of the tank and the corresponding level in the console

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines