import subprocess
import time
import zmq

context = zmq.Context()
socket = context.socket(zmq.SUB)
socket.connect('tcp://localhost:5556')
socket.setsockopt(zmq.SUBSCRIBE, b'')

default = 0
attack = 0


while True:
    message = socket.recv()
    message = message.decode("utf-8") 
    print(message)
    if(message == '0' and default == 0):
        default = 1
        if(attack == 1):
            p.terminate()
            attack = 0 
        d = subprocess.Popen(['python', 'EPANETCPA_default.py'])
        
    elif(message == '1' and default == 1):
        d.terminate()
        default = 0
        p = subprocess.Popen(['python', 'EPANETCPA_attack01.py'])
        attack = 1
        
    else:
        pass
