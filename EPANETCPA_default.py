import zmq
import time

timer = 0;

#NetMQ
context = zmq.Context()
socket = context.socket(zmq.PUB)
socket.bind("tcp://*:5555")

while True:
    #Send
    socket.send_string("%s %d" %("time", timer))
    print("%s %d" %("time", timer))
    time.sleep(0.02)
    socket.send_string("%s %d.2" %("node1", 0))
    print("%s %d.2" %("node1", 0))
    time.sleep(0.02)
    socket.send_string("%s %d.2" %("pump1", 0))
    print("%s %d.2" %("pump1", 0))
    time.sleep(0.02)
    socket.send_string("%s %d.2" %("pump2", 0))
    print("%s %d.2" %("pump2", 0))

    timer = timer + 1
    
    if timer > 684:
        timer = 0
        
    time.sleep(0.05)
