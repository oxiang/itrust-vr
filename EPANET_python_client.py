from  epanettools.epanettools import EPANetSimulation, Node, Link, Network, Nodes, Links, Patterns, Pattern, Controls, Control # import all elements needed
import zmq
import time

#Loading Simulation file
file = "C:\\Users\\sceps\\Desktop\\epatut.inp"
es = EPANetSimulation(file)

#Nodes
number_nodes = len(es.network.nodes)
nodes_id = []
n = es.network.nodes
for i in range(1,number_nodes + 1):
    nodes_id.append(n[i].id)
print("Node IDs {}".format(nodes_id))
print("Number of nodes {}".format(number_nodes))

#Links
number_links = len(es.network.links)
link_id = []
l = es.network.links
for i in range(1, number_links + 1):
    link_id.append(l[i].id)
print("Link IDs {}".format(link_id))
print("Number of Link {}".format(number_links))

es.run()

#Properties of nodes
p = Node.value_type['EN_PRESSURE']
node_pressures = []
for i in range(number_nodes):
    node_pressures.append(n[nodes_id[i]].results[p])

#Properties of link
f = Link.value_type['EN_FLOW']
link_flow = []
for i in range(number_links):
    link_flow.append(l[link_id[i]].results[f])

#NetMQ
context = zmq.Context()
socket = context.socket(zmq.REQ)
socket.bind("tcp://*:5555")

for i in range(177):
    print('sending request..')
    data = node_pressures[0][i]
    socket.send(b"%d.2" %(data))

    message = socket.recv()
    print(message)
    time.sleep(1)

    
