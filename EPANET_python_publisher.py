from  epanettools.epanettools import EPANetSimulation, Node, Link, Network, Nodes, Links, Patterns, Pattern, Controls, Control # import all elements needed
import zmq
import time

#Loading Simulation file
file = "C:\\Users\\sceps\\Desktop\\epatut.inp"
es = EPANetSimulation(file)

#Nodes
number_nodes = len(es.network.nodes)
nodes_id = []
n = es.network.nodes
for i in range(1,number_nodes + 1):
    nodes_id.append(n[i].id)
print("Node IDs {}".format(nodes_id))
print("Number of nodes {}".format(number_nodes))

#Links
number_links = len(es.network.links)
link_id = []
l = es.network.links
for i in range(1, number_links + 1):
    link_id.append(l[i].id)
print("Link IDs {}".format(link_id))
print("Number of Link {}".format(number_links))

es.run()

#Properties of nodes
p = Node.value_type['EN_PRESSURE']
node_pressures = []
for i in range(number_nodes):
    node_pressures.append(n[nodes_id[i]].results[p])

#Properties of link
f = Link.value_type['EN_FLOW']
link_flow = []
for i in range(number_links):
    link_flow.append(l[link_id[i]].results[f])

#NetMQ
context = zmq.Context()
socket = context.socket(zmq.PUB)
socket.bind("tcp://*:5555")

#Topic address
topic = 1011

##for i in range(177):
##    print('sending request..')
##    data = node_pressures[0][i]
##    flow = link_flow[1][i]
##    
##    socket.send_string("%d %d.2 %d.2" %(topic,data,flow))
##    print("data sent %d %d.2 %d.2" %(topic,data,flow))
##    time.sleep(1)

def senddata():
    i = 0
    for i in range(177):
        #send timing
        print(i);
        socket.send_string("%s %d.2" %("timing", i))
        #Send nodes data
        print('sending request')
        socket.send_string("%s %d.2" %("node2", node_pressures[0][i]))
        print("%s %d.2" %("node2", node_pressures[0][i]))
        socket.send_string("%s %d.2" %("node3", node_pressures[1][i]))
        print("%s %d.2" %("node3", node_pressures[1][i]))
        socket.send_string("%s %d.2" %("node7", node_pressures[2][i]))
        print("%s %d.2" %("node7", node_pressures[2][i]))
        socket.send_string("%s %d.2" %("node4", node_pressures[3][i]))
        print("%s %d.2" %("node4", node_pressures[3][i]))
        socket.send_string("%s %d.2" %("node6", node_pressures[4][i]))
        print("%s %d.2" %("node6", node_pressures[4][i]))
        socket.send_string("%s %d.2" %("node5", node_pressures[5][i]))
        print("%s %d.2" %("node5", node_pressures[5][i]))
        socket.send_string("%s %d.2" %("node1", node_pressures[6][i]))
        print("%s %d.2" %("node1", node_pressures[6][i]))
        socket.send_string("%s %d.2" %("node8", node_pressures[7][i]))
        print("%s %d.2" %("node8", node_pressures[7][i]))

        #Send pipe data
        socket.send_string("%s %d.2" %("pipe1", link_flow[0][i]))
        print("%s %d.2" %("pipe1", link_flow[0][i]))
        socket.send_string("%s %d.2" %("pipe2", link_flow[1][i]))
        print("%s %d.2" %("pipe2", link_flow[1][i]))
        socket.send_string("%s %d.2" %("pipe6", link_flow[2][i]))
        print("%s %d.2" %("pipe6", link_flow[2][i]))
        socket.send_string("%s %d.2" %("pipe3", link_flow[3][i]))
        print("%s %d.2" %("pipe3", link_flow[3][i]))
        socket.send_string("%s %d.2" %("pipe7", link_flow[4][i]))
        print("%s %d.2" %("pipe7", link_flow[4][i]))
        socket.send_string("%s %d.2" %("pipe4", link_flow[5][i]))
        print("%s %d.2" %("pipe4", link_flow[5][i]))
        socket.send_string("%s %d.2" %("pipe5", link_flow[6][i]))
        print("%s %d.2" %("pipe5", link_flow[6][i]))
        socket.send_string("%s %d.2" %("pipe8", link_flow[7][i]))
        print("%s %d.2" %("pipe8", link_flow[7][i]))
        socket.send_string("%s %d.2" %("pipe9", link_flow[8][i]))
        print("%s %d.2" %("pipe9", link_flow[8][i]))
        i = i+1
        time.sleep(1)
    
while True:
    senddata()
