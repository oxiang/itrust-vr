﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Pipes : MonoBehaviour
{
    public TestMQ mainMQ;
    public float currentval;
    public string topic;

    float threshold;
    float threshold2;
    float threshold3;
    float threshold4;
    Color _matcolor;
    string[] newDatas;
    Material _material;
    Color lerpedColor;
    // Start is called before the first frame update
    void Start()
    {
        mainMQ = Camera.main.GetComponent<TestMQ>();
        _material = GetComponent<Renderer>().material;
        currentval = 420;
        threshold = 100;
        threshold2 = 75;
        threshold3 = 50;
        threshold4 = 25;
        //topic = "pipe1";
    }

    // Update is called once per frame
    void Update()
    {
        //comment this line if test without the communcation script
        //currentval = mainMQ.piper;
        newDatas = mainMQ.values;
        if(newDatas[0] == topic)
        {
            currentval = float.Parse(newDatas[1]);
        }

        //Decide threshold colors
        if (currentval > threshold)
        {
            _matcolor = Color.red;
        }
        else if (currentval > threshold2)
        {
            _matcolor = Color.yellow;
        }
        else if (currentval > threshold3)
        {
            _matcolor = Color.green;
        }
        else if (currentval > threshold4)
        {
            _matcolor = Color.cyan;
        }
        else
        {
            _matcolor = Color.blue;
        }

        Debug.Log("Chaning color");
        lerpedColor = Color.Lerp(Color.white, _matcolor, Mathf.PingPong(Time.time, 1));
        _material.color = lerpedColor;
    }
}
