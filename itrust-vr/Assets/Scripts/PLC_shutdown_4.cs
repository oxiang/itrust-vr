﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PLC_shutdown_4 : MonoBehaviour
{

    public Scenario4_4 sc4;
    public int activation_stage;
    public int next_stage;

    void Start()
    {
        
    }


    void Update()
    {

    }

    private void OnTriggerEnter(Collider other)
    {
        if (sc4.stage == activation_stage)
        {
            sc4.stage = next_stage;
            Debug.Log("Next stage = " + next_stage.ToString());
        }
    }
}
