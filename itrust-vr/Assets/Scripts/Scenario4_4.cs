﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;

public class Scenario4_4 : MonoBehaviour
{
    public TestMQ mainMQ;
    public string topic;
    public GameObject particle4;
    public GameObject _centerText;
    public earthquake_cam _ec;
    public GameObject spawner04;
    public Simulation_4 _simulation;

    public ParticleSystem _waterspray;
    public ParticleSystem _waterspray2;

    public AudioSource _explosionBGM;
    public AudioSource _goodEnding;

    public float completion;

    Text _text;
    string[] newDatas;
    public float currentval;
    float hintTime = 40;
    float endTime = 50;

    public bool hint;
    public bool end;

    public int stage;
    public GameObject plc1_particle;
    public GameObject plc2_particle;
    public GameObject plc4_particle;

    public Tank_4 t1;
    public Tank_4 t2;
    public Tank_4 t4;

    public GameObject hack_hint;
    public GameObject _simulation_ui;
    public Text text_ui;

    private void OnEnable()
    {
        _simulation_ui.SetActive(true);
        hack_hint.SetActive(false);
        currentval = 0;
        stage = 0;
        hint = true;
        end = true;
        spawner04.SetActive(true);
        _waterspray.Stop();
        _waterspray2.Stop();;
        StartCoroutine(Message("Find the hacker before it's too late!"));
    }

    private void OnDisable()
    {
        hint = true;
        end = true;
        spawner04.SetActive(false);
    }

    void Start()
    {
        completion = 0;
        _text = _centerText.GetComponentInChildren<Text>();
        StartCoroutine(Message("Find the hacker before it's too late!"));
    }

    // Update is called once per frame
    void Update()
    {
        newDatas = mainMQ.values;
        if (newDatas[0] == topic)
        {
            currentval = float.Parse(newDatas[1]);
        }

        if(currentval > hintTime && hint && _simulation.state == 4)
        {
            ActivateHint();
        }

        if(currentval > endTime && end && _simulation.state == 4)
        {
            ActivateEnd();
        }
        if(stage == 0)
        {
            text_ui.text = "Find the TANK with DANGER and disable it by going near it\n go through the red portal to move underground";
            // PLC 1 
            plc1_particle.SetActive(true);
            plc2_particle.SetActive(false);
            plc4_particle.SetActive(false);
            t1.danger = "DANGER";
            t2.danger = "Normal";
            t4.danger = "Normal";
        }
        if(stage == 1)
        {
            text_ui.text = "Find the TANK with DANGER and disable it";
            // PLC 2
            plc1_particle.SetActive(false);
            plc2_particle.SetActive(true);
            plc4_particle.SetActive(false);
            t1.danger = "Normal";
            t2.danger = "DANGER";
            t4.danger = "Normal";
        }
        if(stage == 2)
        {
            text_ui.text = "Good Job! Find the last TANK with DANGER and disable it";
            // plc 4
            plc1_particle.SetActive(false);
            plc2_particle.SetActive(false);
            plc4_particle.SetActive(true);
            t1.danger = "Normal";
            t2.danger = "Normal";
            t4.danger = "DANGER";
        }
        if(stage == 3)
        {
            text_ui.text = "Trace the beam and find the hacker! \n Click on the hacker to disable him";
            hack_hint.SetActive(true);
        }
    }

    public void ActivateHint()
    {
        particle4.SetActive(true);
        StartCoroutine(Message("Hurry up and find the hacker!"));
        hint = false;
    }

    public IEnumerator Message(string msg)
    {
        
        _text.text = msg;
        _centerText.SetActive(true);
        yield return new WaitForSeconds(5f);
        _centerText.SetActive(false);
        _waterspray.Stop();
        _waterspray2.Stop();
        _explosionBGM.Stop();
        _ec.enabled = false;
    }

    public void ActivateEnd()
    {
        _simulation_ui.SetActive(false);
        hack_hint.SetActive(false);
        t1.danger = "Normal";
        t2.danger = "Normal";
        t4.danger = "Normal";
        plc1_particle.SetActive(false);
        plc2_particle.SetActive(false);
        plc4_particle.SetActive(false);
        _explosionBGM.Play();
        particle4.SetActive(false);
        _ec.enabled = true;
        _waterspray.Play();
        _waterspray2.Play();
        StartCoroutine(Message("You were too slow, the hacker won. Tank exploded"));
        end = false;
        spawner04.SetActive(false);
        _simulation.DeactivateSim();
        this.enabled = false;
    }

    public void HappyEnd()
    {
        _simulation_ui.SetActive(false);
        hack_hint.SetActive(false);
        t1.danger = "Normal";
        t2.danger = "Normal";
        t4.danger = "Normal";
        plc1_particle.SetActive(false);
        plc2_particle.SetActive(false);
        plc4_particle.SetActive(false);
        _goodEnding.Play();
        particle4.SetActive(false);
        StartCoroutine(Message("Congrats! You stopped the hack!"));
        end = false;
        _ec.enabled = false;
        spawner04.SetActive(false);
        _simulation.DeactivateSim();
        hint = true;
        end = true;
        this.enabled = false;
    }
}
