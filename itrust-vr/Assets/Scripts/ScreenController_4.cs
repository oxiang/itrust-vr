﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;

public class ScreenController_4 : MonoBehaviour
{
    public GameObject _centermessage;
    public GameObject _loginParticle;

    Text _text;
    bool _welcome;
    bool _message;
    AudioSource _as;
    // Start is called before the first frame update
    void Start()
    {
        _as = GetComponent<AudioSource>();
        _text = _centermessage.GetComponentInChildren<Text>();
        StartCoroutine(WelcomeMessage());       
    }

    // Update is called once per frame
    void Update()
    {

        
    }

    public IEnumerator WelcomeMessage()
    {
        _loginParticle.SetActive(true);
        _centermessage.SetActive(true);
        StartCoroutine(AnimateText("Welcome to iTrust World"));
        yield return new WaitForSeconds(3f);
        _centermessage.SetActive(false);
        yield return new WaitForSeconds(1f);
        _loginParticle.SetActive(false);
        StartCoroutine(InstructorMessage());
        
    }

    public IEnumerator InstructorMessage()
    {
        _centermessage.SetActive(true);
        StartCoroutine(AnimateText("To access to simulation, CLICK the control panel"));
        yield return new WaitForSeconds(3f);
        StartCoroutine(AnimateText("Right click to switch from NORMAL view to Water System view"));
        yield return new WaitForSeconds(3f);
        _centermessage.SetActive(false);
        yield return new WaitForSeconds(1f);       
    }



    IEnumerator AnimateText(string msg)
    {
        _as.Play();
        for(int i =0; i< msg.Length+1; i++)
        {
            _text.text = msg.Substring(0, i);
            yield return new WaitForSeconds(0.03f);
        }
        _as.Stop();
    }
}
