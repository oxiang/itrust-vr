﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Tanks : MonoBehaviour
{
    public earthquake_cam _em;
    public ParticleSystem _waterParticle;
    public ParticleSystem _waterParticle2;
    public GameObject _as;
    public TestMQ mainMQ;
    public Transform waterlevel;
    public float levelDifference;
    public float maxYLevel;
    public float newData;
    public string[] newDatas;
    public float speed;
    public float maxPressure;
    public float minPressure;
    public Vector3 newLevel;
    public string topic;

    //List<double> values;

    // Start is called before the first frame update
    void Start()
    {
        mainMQ = Camera.main.GetComponent<TestMQ>();
        //maxPressure = 100f; //69 as default node1
        //minPressure = 0f; //63 as deault node1
        maxYLevel = waterlevel.position.y;
        speed = 1f;
        levelDifference = 6;
        //newData = 69;
        //topic = "node1";

        newLevel = new Vector3(waterlevel.position.x, maxYLevel - levelDifference, waterlevel.position.z);
    }

    void Update()
    {
        //newData = mainMQ.value;
        newDatas = mainMQ.values;
        if(newDatas[0] == topic)
        {
            newData = float.Parse(newDatas[1]);
        }
        float inputData = newData;
        movement(inputData);

        if(newData > maxPressure)
        {
            _em.enabled = true;
            _as.GetComponent<AudioSource>().enabled = true;
            Debug.Log("sound is playing");
            _waterParticle.Play();
            _waterParticle2.Play();
        }
        else
        {
            _em.enabled = false;
            _as.GetComponent<AudioSource>().enabled = false;
            _waterParticle.Stop();
            _waterParticle2.Stop();
        }
    }

    void movement(float input)
    {
        if(input < minPressure)
        {
            input = minPressure;
        }
        else if (input > maxPressure)
        {
            input = maxPressure;
        }

        float ratio = 1 - ((input-minPressure) / (maxPressure- minPressure));
        float newYlevel = maxYLevel - (ratio * levelDifference);
        float step = speed * Time.deltaTime;
        Vector3 newposition = new Vector3(waterlevel.position.x, newYlevel, waterlevel.position.z);
        waterlevel.position = Vector3.MoveTowards(waterlevel.position, newposition, step);
    }

}
