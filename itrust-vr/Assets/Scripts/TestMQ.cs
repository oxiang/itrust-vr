﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Threading;
using System.Timers;
using NetMQ;
using NetMQ.Sockets;


public class TestMQ : MonoBehaviour
{
    //public float value = 69;
    //public float piper = 380;
    public string[] values;
    Thread client_thread_;
    private Object thisLock_ = new Object();
    public string send;
    bool stop_thread_ = false;
    string topic = "";

    void Start()
    {
        Debug.Log("Start a request thread.");
        client_thread_ = new Thread(NetMQClient);
        client_thread_.Start();
        send = "00";
    }

    void NetMQClient()
    {
        AsyncIO.ForceDotNet.Force();
        NetMQConfig.Cleanup();

        //string topic = "KV6XML";
        using (var pubSocket = new RequestSocket())
        //using (var subSocket = new SubscriberSocket("tcp://localhost:5555"))
        {
            //subSocket.Options.ReceiveHighWatermark = 1000;
            //subSocket.Subscribe(topic);
            Debug.Log("Subscriber socket connecting...");
            pubSocket.Connect("tcp://localhost:5555");
            pubSocket.Options.ReceiveHighWatermark = 1000;
            while (!stop_thread_)
            {
                //Debug.Log("waiting...?");
                //string messageTopicReceived = subSocket.ReceiveFrameString();
                //string[] messageReceived = messageTopicReceived.Split(' ');

                //Slicing and getting the value
                //value = float.Parse(messageReceived[1]);
                //piper = float.Parse(messageReceived[2]);
                //values = messageReceived;
                //values = messageReceived;
                //Debug.Log(messageReceived[0] + " and " + messageReceived[1]);

                pubSocket.SendFrame(send);
                //Debug.Log("sending " + send);
                string messageTopicReceived = pubSocket.ReceiveFrameString();
                string[] messageReceived = messageTopicReceived.Split(' ');
                values = messageReceived;
                //Debug.Log(messageReceived[0] + " " + messageReceived[1]);
            }
            //subSocket.Disconnect("tcp://localhost:5555");
            pubSocket.Disconnect("tcp://localhost:5555");
        }
        Debug.Log("ContextTerminate.");
        NetMQConfig.Cleanup();
    }

    void Update()
    {
        
    }

    void OnApplicationQuit()
    {
        lock (thisLock_) stop_thread_ = true;
        client_thread_.Join();
        Debug.Log("Quit the thread.");

    }
}
