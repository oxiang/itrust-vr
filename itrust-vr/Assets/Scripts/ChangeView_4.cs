﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;

public class ChangeView_4 : MonoBehaviour
{
    public TestMQ mainMQ;
    public string topic;
    public double hours;
    public double minutes;
    public AudioSource watersystem;
    public AudioSource town;
    public Text timeText;
    public Text mText;
    public GameObject _raycast_object;

    public string[] newDatas;
    public float currentval;
    Camera cam;
    public int state = 1;
    // Start is called before the first frame update
    void Start()
    {
        //mainMQ = Camera.main.GetComponent<TestMQ>();
        cam = Camera.main;
        currentval = 0;
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetMouseButtonDown(1))
        {
            if (state == 1)
            {
                Debug.Log("Changing view to hide");
                Hide();
            }
            else if (state == 2)
            {
                Debug.Log("Changing view to show all");
                ShowAll();
            }
        }

        newDatas = mainMQ.values;
        if (newDatas[0] == topic)
        {
            currentval = float.Parse(newDatas[1]);
        }

        ChangeTime(currentval);
    }

    private void Hide()
    {
        _raycast_object.SetActive(true);
        watersystem.volume = 1;
        town.volume = 0.2f;
        mText.text = "Water System View";
        cam.cullingMask = ~(1 << LayerMask.NameToLayer("RealWorld"));
        state = 2;
    }

    private void ShowAll()
    {
        _raycast_object.SetActive(false);
        watersystem.volume = 0;
        town.volume = 1;
        mText.text = "Normal View";
        cam.cullingMask = -1;
        state = 1;
    }

    private void ChangeTime(float i)
    {
        //hours = Math.Floor(i/178 * 24);
        //minutes = (Math.Round((i / 178 * 24) -hours, 2))*100 ;
        //timeText.text = "Time: " + hours.ToString() + ":" + minutes.ToString();
        timeText.text = "Time Elapsed: " + i.ToString() + " hrs";
    }
}
