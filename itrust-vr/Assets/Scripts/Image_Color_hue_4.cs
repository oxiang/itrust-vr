﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Image_Color_hue_4 : MonoBehaviour
{
    public float Speed = 1;
    public Image _image; 

    void Start()
    {
        
    }

    
    void Update()
    {
        _image.color = HSBColor.ToColor(new HSBColor(Mathf.PingPong(Time.time * Speed, 1), 1, 1));
        Debug.Log("Changing color");
    }
}
