﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Pumps : MonoBehaviour
{
    public TestMQ mainMQ;
    public GameObject leftPipe;
    public GameObject rightPipe;
    public GameObject core;
    public GameObject leftFan;
    public GameObject rightFan;
    public string topic;
    public float currentval;
    public string danger;

    Material left;
    Material right;
    Material centre;
    Color _mat;
    string[] newDatas;
    float threshold;
    float threshold2;


    // Start is called before the first frame update
    void Start()
    {
        danger = "Normal";
        left = leftPipe.GetComponent<Renderer>().material;
        right = rightPipe.GetComponent<Renderer>().material;
        centre = core.GetComponent<Renderer>().material;
        threshold = 100;
        threshold2 = 20;
    }

    // Update is called once per frame
    void Update()
    {
        newDatas = mainMQ.values;
        if (newDatas[0] == topic)
        {
            currentval = float.Parse(newDatas[1]);
        }
        
        if(currentval > threshold)
        {
            _mat = Color.red;
        }
        else if(currentval < threshold2)
        {
            _mat = Color.grey;
        }
        else
        {
            _mat = Color.green;
        }

        Spin(currentval);
        Color lerpedColor = Color.Lerp(Color.white, _mat, Mathf.PingPong(Time.time, 1));
        left.color = lerpedColor;
        right.color = lerpedColor;
        centre.color = lerpedColor;
    }

    private void Spin(float rate)
    {
        leftFan.transform.Rotate(Vector3.forward * Time.deltaTime * rate);
        rightFan.transform.Rotate(Vector3.forward * Time.deltaTime * rate);
    }

}
