﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Cam_orbit : MonoBehaviour
{
    // Start is called before the first frame update
    public GameObject target;
    public float OrbitDegrees = 1f;
    Camera _cam;
    Transform target_xform;

    void Start()
    {
        _cam = GetComponent<Camera>();
        target_xform = target.GetComponent<Transform>();
    }

    // Update is called once per frame
    void Update()
    {
        transform.LookAt(target_xform);
        transform.position = RotatePointAroundPivot(transform.position,
                           target_xform.position,
                           Quaternion.Euler(0, OrbitDegrees * Time.deltaTime, 0));
    }

    public static Vector3 RotatePointAroundPivot(Vector3 point, Vector3 pivot, Quaternion angle)
    {
        return angle * (point - pivot) + pivot;
    }
}
