﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;

using UnityEngine;

public class flickering_warning : MonoBehaviour
{
    RectTransform _rt;
    public GameObject _text;
    float height = 100;
    Image _c;
    Color lerpedColor;

    // Start is called before the first frame update
    void OnEnable()
    {
        Debug.Log("Activated the panel");
        _rt = GetComponent<RectTransform>();
        _rt.sizeDelta = new Vector2(800, 20);
        StartCoroutine(Flick());
        _c = GetComponent<Image>();
    }

    private void OnDisable()
    {
        _text.SetActive(false);
    }

    // Update is called once per frame
    void Update()
    {
        Debug.Log("Chaning warning sign color");
        lerpedColor = Color.Lerp(Color.white, Color.red, Mathf.PingPong(Time.time, 1));
        _c.color = lerpedColor;
    }

    IEnumerator Flick()
    {
        for (int i = 20; i < height; i=i+5)
        {
            Debug.Log("Setting the size");
            _rt.sizeDelta = new Vector2(800, i);
            yield return new WaitForSeconds(.001f);
            _text.SetActive(true);
        }
    }
}
