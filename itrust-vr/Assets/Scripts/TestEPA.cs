﻿using EpanetCSharpLibrary;
using System;
using System.Runtime.InteropServices;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TestEPA : MonoBehaviour
{
    #region Epanet Imports
    public delegate void UserSuppliedFunction(string param0);
    [DllImport("epanet2.dll", EntryPoint = "ENepanet")]
    public static extern int ENepanet(string f1, string f2, string f3, UserSuppliedFunction vfunc);
    //public static extern int ENgetnodeindex(string id, ref int index);
    // Start is called before the first frame update
    void Start()
    {
        int i = 0;
        //int v = 0;
        string f1 = "", f2 = "", f3 = "";
        ////get the DLL's version
        //i = Epanet.ENgetversion(ref v);
        //Debug.Log("Connect to epanet");

        f1 = "Net1.inp";
        f2 = "out.txt";
        f3 = "rep.txt";
        //run an EPANet simulation
        i = ENepanet(f1, f2, f3, null);
        Debug.Log("epanet does run?");
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    #endregion
}
