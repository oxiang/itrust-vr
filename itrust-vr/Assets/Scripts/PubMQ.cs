﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Threading;
using NetMQ;
using NetMQ.Sockets;

public class PubMQ : MonoBehaviour
{
    Thread client_thread_;
    private Object thisLock_ = new Object();
    bool stop_thread_ = false;
    public string send;

    void Start()
    {
        Debug.Log("Start a request thread.");
        client_thread_ = new Thread(NetMQClient);
        client_thread_.Start();
        send = "00";
    }


    void NetMQClient()
    {
        AsyncIO.ForceDotNet.Force();
        NetMQConfig.Cleanup();


        using (var pubSocket = new PublisherSocket())
        {
            Debug.Log("Publisher socket publishing...");
            pubSocket.Bind("tcp://*:5556");
            pubSocket.Options.ReceiveHighWatermark = 1000;
            while (!stop_thread_)
            {
                pubSocket.SendFrame(send);
                Debug.Log("sending " + send);
                Thread.Sleep(500);
            }
            pubSocket.Disconnect("tcp://*:5556");
        }
        Debug.Log("ContextTerminate.");
        NetMQConfig.Cleanup();
    }

    void Update()
    {

    }

    void OnApplicationQuit()
    {
        lock (thisLock_) stop_thread_ = true;
        client_thread_.Join();
        Debug.Log("Quit the thread.");

    }
}
