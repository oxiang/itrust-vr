﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityStandardAssets.Characters.FirstPerson;
using UnityEngine.UI;

public class Raycast_4 : MonoBehaviour
{
    public GameObject _info;
    public ChangeView_4 _cv4;
    public GameObject fpsController;
    public Canvas _maincanvas;
    public Canvas _simulationcanvas;
    public string _name;
    public Simulation_4 sim;
    public Scenario4_4 sim4;
    public Scenario3_4 sim3;

    public Text _text;
    public Tank_4 _tankScript;
    public Pumps _pump;
    void Start()
    {
        Cursor.lockState = CursorLockMode.Locked;
        Cursor.visible = false;
        _simulationcanvas.enabled = false;
    }

    void Update()
    {
        RaycastHit[] hit;
        Ray landingRay = new Ray(transform.position, transform.forward);

        hit = Physics.RaycastAll(transform.position, transform.forward, Mathf.Infinity);
        Debug.DrawRay(transform.position, transform.forward * 1000, Color.red);
        for (int i =0; i< hit.Length; i++)
        {
            if (hit[i].collider.tag == "panel" && _cv4.state == 1)
            {
                _name = hit[i].collider.gameObject.name;
                _info.SetActive(true);
                _text.text = _name;
                i = hit.Length;
                if (Input.GetMouseButtonDown(0))
                {
                    fpsController.GetComponent<RigidbodyFirstPersonController>().enabled = false;
                    fpsController.GetComponent<ChangeView_4>().enabled = false;
                    _maincanvas.enabled = false;
                    Cursor.lockState = CursorLockMode.None;
                    Cursor.visible = true;
                    _simulationcanvas.enabled = true;
                }
                else if (Input.GetKey(KeyCode.Escape))
                {
                    fpsController.GetComponent<RigidbodyFirstPersonController>().enabled = true;
                    fpsController.GetComponent<RigidbodyFirstPersonController>().mouseLook.lockCursor = true;
                    fpsController.GetComponent<ChangeView_4>().enabled = true;
                    _maincanvas.enabled = true;
                    //Cursor.lockState = CursorLockMode.Locked;
                    //Cursor.visible = false;
                    _simulationcanvas.enabled = false;                    
                }

            }
            else if (hit[i].collider.tag == "tank" && _cv4.state == 2)
            {
                Debug.Log("Found a tank");
                _name = hit[i].collider.transform.parent.gameObject.name;
                _tankScript = hit[i].collider.transform.parent.gameObject.GetComponent<Tank_4>();
                _info.SetActive(true);
                _text.text = "Tank\n\nName: " + _name + "\nWater Level: " + _tankScript.newData.ToString() + "\n\nStatus: " + _tankScript.danger;
                i = hit.Length;
            }
            else if(hit[i].collider.tag == "pump" && _cv4.state == 2)
            {
                Debug.Log("Found a pump");
                _name = hit[i].collider.transform.parent.gameObject.name;
                _pump = hit[i].collider.transform.parent.gameObject.GetComponent<Pumps>();
                _info.SetActive(true);
                _text.text = "Pump\n\nName: " + _name + "\nFlow Rate: " + _pump.currentval.ToString() + "\n\nStatus: " + _pump.danger;
                i = hit.Length;
            }
            else if (hit[i].collider.tag == "scenario4" && sim.state == 4)
            {
                if (Input.GetMouseButtonDown(0))
                {
                    Debug.Log("completed simulation 4");
                    sim4.HappyEnd();
                }
                    
            }
            else if (hit[i].collider.tag == "scenario3" && sim.state == 3)
            {
                if (Input.GetMouseButtonDown(0))
                {
                    Debug.Log("completed simulation 3");
                    sim3.HappyEnd();
                }

            }
            else
            {
                _info.SetActive(false);
            }
        }
    }
}
