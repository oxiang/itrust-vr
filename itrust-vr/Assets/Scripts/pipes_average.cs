﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class pipes_average : MonoBehaviour
{
    public Pumps _pump1;
    public Pumps _pump2;

    float val1;
    float val2;
    public float val;
    float threshold1;
    float threshold2;
    Material material;
    Color _mat;
    // Start is called before the first frame update
    void Start()
    {
        material = GetComponent<Renderer>().material;
        val1 = 0;
        val2 = 0;
        threshold1 = 100;
        threshold2 = 20;
    }

    // Update is called once per frame
    void Update()
    {
        val1 = _pump1.currentval;
        val2 = _pump2.currentval;
        val = (val1 + val2) / 2;
        if (val > threshold1)
        {
            _mat = Color.red;
        }
        else if (val < threshold2)
        {
            _mat = Color.grey;
        }
        else
        {
            _mat = Color.green;
        }

        Color lerpedColor = Color.Lerp(Color.white, _mat, Mathf.PingPong(Time.time, 1));
        material.color = lerpedColor;
    }
}
