﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Panel_activation : MonoBehaviour
{
    public PubMQ _pubmq;
    public GameObject warningSign;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        RaycastHit hit;
        Ray landingRay = new Ray(transform.position, transform.forward);

        Debug.DrawRay(transform.position, transform.forward * 1000, Color.red);
        Debug.Log("raycast");
        if(Physics.Raycast(landingRay, out hit, Mathf.Infinity))
        {
            
            if(hit.collider.tag == "panel")
            {
                warningSign.SetActive(true);
                Debug.Log("Activating panel");
            }
            else
            {
                warningSign.SetActive(false);
                Debug.Log("Deactivating panel");
            }

        }
    }
}
