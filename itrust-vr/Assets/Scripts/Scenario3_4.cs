﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Scenario3_4 : MonoBehaviour
{
    public TestMQ mainMQ;
    public string topic;
    public GameObject particle3;
    public GameObject _centerText;
    public earthquake_cam _ec;
    public GameObject spawner03;
    public Simulation_4 _simulation;

    public GameObject _explosion1;
    public GameObject _explosion2;

    public AudioSource _explosionBGM;
    public AudioSource _goodEnding;

    public float completion;

    Text _text;
    string[] newDatas;
    public float currentval;
    float hintTime = 5;
    float endTime = 10;

    public bool hint;
    public bool end;

    private void OnEnable()
    {
        currentval = 0;
        hint = true;
        end = true;
        spawner03.SetActive(true);
        _explosion1.SetActive(false);
        _explosion2.SetActive(false);
        StartCoroutine(Message("Find the hacker before it's too late!"));
    }

    private void OnDisable()
    {
        hint = true;
        end = true;
        spawner03.SetActive(false);
    }

    void Start()
    {
        completion = 0;
        _text = _centerText.GetComponentInChildren<Text>();
        StartCoroutine(Message("Find the hacker before it's too late!"));
    }

    // Update is called once per frame
    void Update()
    {
        newDatas = mainMQ.values;
        if (newDatas[0] == topic)
        {
            currentval = float.Parse(newDatas[1]);
        }

        if (currentval > hintTime && hint && _simulation.state == 3)
        {
            ActivateHint();
        }

        if (currentval > endTime && end && _simulation.state == 3)
        {
            ActivateEnd();
        }
    }

    public void ActivateHint()
    {
        particle3.SetActive(true);
        StartCoroutine(Message("Hurry up and find the hacker!"));
        hint = false;
    }

    public IEnumerator Message(string msg)
    {
        _text.text = msg;
        _centerText.SetActive(true);
        yield return new WaitForSeconds(5f);
        _centerText.SetActive(false);
        _explosion1.SetActive(false);
        _explosion2.SetActive(false);
        _explosionBGM.Stop();
        _ec.enabled = false;
    }

    public void ActivateEnd()
    {
        _explosionBGM.Play();
        particle3.SetActive(false);
        _ec.enabled = true;
        _explosion1.SetActive(true);
        _explosion2.SetActive(true);
        StartCoroutine(Message("You were too slow, the hacker won. Pump exploded"));
        end = false;
        spawner03.SetActive(false);
        _simulation.DeactivateSim();
        this.enabled = false;
    }

    public void HappyEnd()
    {
        _goodEnding.Play();
        particle3.SetActive(false);
        StartCoroutine(Message("Congrats! You stopped the hack!"));
        end = false;
        _ec.enabled = false;
        spawner03.SetActive(false);
        _simulation.DeactivateSim();
        hint = true;
        end = true;
        this.enabled = false;
    }
}
