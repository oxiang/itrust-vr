﻿using System.Collections;
using System.Collections.Generic;
using UnityStandardAssets.Characters.FirstPerson;
using UnityEngine;
using UnityEngine.UI;

public class Simulation_4 : MonoBehaviour
{


    public GameObject spawner04;
    public GameObject fpsController;
    public Canvas _maincanvas;
    public Canvas _simulationcanvas;
    //public PubMQ _pubmq;
    public TestMQ _testmq;
    public Text _text;
    public Scenario4_4 _s4;
    public Scenario3_4 _s3;
    public AudioSource _mainMisisonBGM;
    


    public float state;

    void Start()
    {
        state = 0;
    }

    // Update is called once per frame
    void Update()
    {
       
    }

    void Switch()
    {
        fpsController.GetComponent<RigidbodyFirstPersonController>().enabled = true;
        fpsController.GetComponent<ChangeView_4>().enabled = true;
        _maincanvas.enabled = true;
        Cursor.visible = false;
        _simulationcanvas.enabled = false;
    }

    public void ActivateSim4()
    {
        if (state == 0)
        {
            _text.text = "Simulation 4";
            _testmq.send = "40";
            Switch();
            state = 4;
            //spawner04.SetActive(true);
            _s4.enabled = true;
            _mainMisisonBGM.Play();
            Debug.Log("Playing background music");
        }
    }

    public void AcitivateSim3()
    {
        if(state == 0)
        {
            _text.text = "Simulation 3";
            _testmq.send = "30";
            Switch();
            state = 3;
            _mainMisisonBGM.Play();
            _s3.enabled = true;
        }      
    }

    public void ActivateSim1()
    {
        if(state == 0)
        {
            _text.text = "Simulation 1";
            _testmq.send = "10";
            Switch();
            state = 1;
        }
    }

    public void DeactivateSim()
    {
        if(state == 1)
        {
            _testmq.send = "00";
            state = 0;
        }
        else if(state == 3)
        {
            _testmq.send = "00";
            state = 0;
            _s3.enabled = false;
        }
        else if (state == 4)
        {
            _testmq.send = "00";
            state = 0;
            //spawner04.SetActive(false);
            _s4.enabled = false;
        }
        _mainMisisonBGM.Stop();
        _text.text = "No Simulation";
    }


}
