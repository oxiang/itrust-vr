﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Threading;
using NetMQ;
using NetMQ.Sockets;

public class REQREPMQ : MonoBehaviour
{
    Thread client_thread_;
    private Object thisLock_ = new Object();
    bool stop_thread_ = false;
    public string send;
    public string[] values;

    public float timeLeft = 5f;

    void Start()
    {
        Debug.Log("Start a request thread.");
        client_thread_ = new Thread(NetMQClient);
        client_thread_.Start();
        send = "00";
    }


    void NetMQClient()
    {
        AsyncIO.ForceDotNet.Force();
        NetMQConfig.Cleanup();


        using (var pubSocket = new RequestSocket())
        {
            Debug.Log("Publisher socket publishing...");
            pubSocket.Connect("tcp://localhost:5555");
            pubSocket.Options.ReceiveHighWatermark = 1000;
            while (!stop_thread_)
            {
                pubSocket.SendFrame(send);
                Debug.Log("sending " + send);
                string messageTopicReceived = pubSocket.ReceiveFrameString();
                string[] messageReceived = messageTopicReceived.Split(' ');
                values = messageReceived;
                Debug.Log(messageReceived[0] + " " + messageReceived[1]);

            }
            pubSocket.Disconnect("tcp://localhost:5555");
        }
        Debug.Log("ContextTerminate.");
        NetMQConfig.Cleanup();
    }

    void Update()
    {
        timeLeft -= Time.deltaTime;
        if(timeLeft < 0 && send == "00")
        {
            send = "40";
            timeLeft = 5f;
        }
        else if (timeLeft < 0 && send == "30")
        {
            send = "00";
            timeLeft = 5f;
        }
    }

    void OnApplicationQuit()
    {
        lock (thisLock_) stop_thread_ = true;
        client_thread_.Join();
        Debug.Log("Quit the thread.");
    }
}
