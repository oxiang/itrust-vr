import pandas as pd
import time
import zmq

#load dataset
truth = pd.read_csv('ctown_no_attacks.csv')
scenario1 = pd.read_csv('scenario01.csv')
scenario3 = pd.read_csv('scenario03.csv')
scenario4 = pd.read_csv('scenario04.csv')

#Setting up network and timer
context = zmq.Context()
socket = context.socket(zmq.REP)
socket.bind("tcp://*:5555")
sleeping_time = 0.00
timer = 0
counter = 0
state = 0

def scenario_0(i):
    print('Scenario 0 {}'.format(i))
    timer = truth.iloc[i, 0]
    t1 = truth['PRESSURE_T1'].iloc[i]
    t2 = truth['PRESSURE_T2'].iloc[i]
    t3 = truth['PRESSURE_T3'].iloc[i]
    t4 = truth['PRESSURE_T4'].iloc[i]
    t5 = truth['PRESSURE_T5'].iloc[i]
    t6 = truth['PRESSURE_T6'].iloc[i]
    t7 = truth['PRESSURE_T7'].iloc[i]
    pu01 = truth['FLOW_PU1'].iloc[i]
    pu02 = truth['FLOW_PU2'].iloc[i]
    pu03 = truth['FLOW_PU3'].iloc[i]
    pu10 = truth['FLOW_PU10'].iloc[i]
    pu11 =truth['FLOW_PU11'].iloc[i]

    if(counter == 0):
        socket.send_string("%s %.2f" %("time", 0))
        print("%s %.2f" %("time", timer))
        time.sleep(sleeping_time)
        
    elif(counter == 1):
        socket.send_string("%s %.2f" %("t1", t1))
        print("%s %.2f" %("t1", t1))
        time.sleep(sleeping_time)
        
    elif(counter == 2):
        socket.send_string("%s %.2f" %("t2", t2))
        print("%s %.2f" %("t2", t2))
        time.sleep(sleeping_time)

    elif(counter == 3):
        socket.send_string("%s %.2f" %("t3", t3))
        print("%s %.2f" %("t3", t3))
        time.sleep(sleeping_time)

    elif(counter == 4):
        socket.send_string("%s %.2f" %("t4", t4))
        print("%s %.2f" %("t4", t4))
        time.sleep(sleeping_time)

    elif(counter == 5):
        socket.send_string("%s %.2f" %("t5", t5))
        print("%s %.2f" %("t5", t5))
        time.sleep(sleeping_time)

    elif(counter == 6):
        socket.send_string("%s %.2f" %("t6", t6))
        print("%s %.2f" %("t6", t6))
        time.sleep(sleeping_time)

    elif(counter == 7):
        socket.send_string("%s %.2f" %("t7", t7))
        print("%s %.2f" %("t7", t7))
        time.sleep(sleeping_time)

    elif(counter == 8):
        socket.send_string("%s %.2f" %("pu01", pu01))
        print("%s %.2f" %("pu01", pu01))
        time.sleep(sleeping_time)

    elif(counter == 9):
        socket.send_string("%s %.2f" %("pu02", pu02))
        print("%s %.2f" %("pu02", pu02))
        time.sleep(sleeping_time)

    elif(counter == 10):
        socket.send_string("%s %.2f" %("pu03", pu03))
        print("%s %.2f" %("pu03", pu03))
        time.sleep(sleeping_time)

    elif(counter == 11):
        socket.send_string("%s %.2f" %("pu10", pu10))
        print("%s %.2f" %("pu10", pu10))
        time.sleep(sleeping_time)

    elif(counter == 12):
        socket.send_string("%s %.2f" %("pu11", pu11))
        print("%s %.2f" %("pu11", pu11))
        time.sleep(sleeping_time)

def scenario_1(i):
    print('scenario 1 {}'.format(i))        
    timer = scenario1.iloc[i, 0]
    t1 = scenario1.iloc[i, 1]
    t2 = scenario1.iloc[i, 2]
    t3 = scenario1.iloc[i, 3]
    t4 = scenario1.iloc[i, 4]
    t5 = scenario1.iloc[i, 5]
    t6 = scenario1.iloc[i, 6]
    t7 = scenario1.iloc[i, 7]
    pu01 = scenario1.iloc[i, 10]
    pu02 = scenario1.iloc[i, 11]
    pu03 = scenario1.iloc[i, 12]
    pu10 = truth['FLOW_PU10'].iloc[i]
    pu11 =truth['FLOW_PU11'].iloc[i]

    if(counter == 0):
        socket.send_string("%s %.2f" %("time", timer))
        print("%s %.2f" %("time", timer))
        time.sleep(sleeping_time)
        
    elif(counter == 1):
        socket.send_string("%s %.2f" %("t1", t1))
        print("%s %.2f" %("t1", t1))
        time.sleep(sleeping_time)
        
    elif(counter == 2):
        socket.send_string("%s %.2f" %("t2", t2))
        print("%s %.2f" %("t2", t2))
        time.sleep(sleeping_time)

    elif(counter == 3):
        socket.send_string("%s %.2f" %("t3", t3))
        print("%s %.2f" %("t3", t3))
        time.sleep(sleeping_time)

    elif(counter == 4):
        socket.send_string("%s %.2f" %("t4", t4))
        print("%s %.2f" %("t4", t4))
        time.sleep(sleeping_time)

    elif(counter == 5):
        socket.send_string("%s %.2f" %("t5", t5))
        print("%s %.2f" %("t5", t5))
        time.sleep(sleeping_time)

    elif(counter == 6):
        socket.send_string("%s %.2f" %("t6", t6))
        print("%s %.2f" %("t6", t6))
        time.sleep(sleeping_time)

    elif(counter == 7):
        socket.send_string("%s %.2f" %("t7", t7))
        print("%s %.2f" %("t7", t7))
        time.sleep(sleeping_time)

    elif(counter == 8):
        socket.send_string("%s %.2f" %("pu01", pu01))
        print("%s %.2f" %("pu01", pu01))
        time.sleep(sleeping_time)

    elif(counter == 9):
        socket.send_string("%s %.2f" %("pu02", pu02))
        print("%s %.2f" %("pu02", pu02))
        time.sleep(sleeping_time)

    elif(counter == 10):
        socket.send_string("%s %.2f" %("pu03", pu03))
        print("%s %.2f" %("pu03", pu03))
        time.sleep(sleeping_time)

    elif(counter == 11):
        socket.send_string("%s %.2f" %("pu10", pu10))
        print("%s %.2f" %("pu10", pu10))
        time.sleep(sleeping_time)

    elif(counter == 12):
        socket.send_string("%s %.2f" %("pu11", pu11))
        print("%s %.2f" %("pu11", pu11))
        time.sleep(sleeping_time)

def scenario_3(i):
    print('scenario 3 {}'.format(i))        
    timer = scenario3.iloc[i, 0]
    t1 = scenario3.iloc[i, 1]
    t2 = scenario3.iloc[i, 2]
    t3 = scenario3.iloc[i, 3]
    t4 = scenario3.iloc[i, 4]
    t5 = scenario3.iloc[i, 5]
    t6 = scenario3.iloc[i, 6]
    t7 = scenario3.iloc[i, 7]
    pu10 = scenario3.iloc[i, 10]
    pu11 = scenario3.iloc[i, 11]
    pu03 = truth['FLOW_PU3'].iloc[i]
    pu01 = truth['FLOW_PU1'].iloc[i]
    pu02 =truth['FLOW_PU2'].iloc[i]

    if(counter == 0):
        socket.send_string("%s %.2f" %("time", timer))
        print("%s %.2f" %("time", timer))
        time.sleep(sleeping_time)
        
    elif(counter == 1):
        socket.send_string("%s %.2f" %("t1", t1))
        print("%s %.2f" %("t1", t1))
        time.sleep(sleeping_time)
        
    elif(counter == 2):
        socket.send_string("%s %.2f" %("t2", t2))
        print("%s %.2f" %("t2", t2))
        time.sleep(sleeping_time)

    elif(counter == 3):
        socket.send_string("%s %.2f" %("t3", t3))
        print("%s %.2f" %("t3", t3))
        time.sleep(sleeping_time)

    elif(counter == 4):
        socket.send_string("%s %.2f" %("t4", t4))
        print("%s %.2f" %("t4", t4))
        time.sleep(sleeping_time)

    elif(counter == 5):
        socket.send_string("%s %.2f" %("t5", t5))
        print("%s %.2f" %("t5", t5))
        time.sleep(sleeping_time)

    elif(counter == 6):
        socket.send_string("%s %.2f" %("t6", t6))
        print("%s %.2f" %("t6", t6))
        time.sleep(sleeping_time)

    elif(counter == 7):
        socket.send_string("%s %.2f" %("t7", t7))
        print("%s %.2f" %("t7", t7))
        time.sleep(sleeping_time)

    elif(counter == 8):
        socket.send_string("%s %.2f" %("pu01", pu01))
        print("%s %.2f" %("pu01", pu01))
        time.sleep(sleeping_time)

    elif(counter == 9):
        socket.send_string("%s %.2f" %("pu02", pu02))
        print("%s %.2f" %("pu02", pu02))
        time.sleep(sleeping_time)

    elif(counter == 10):
        socket.send_string("%s %.2f" %("pu03", pu03))
        print("%s %.2f" %("pu03", pu03))
        time.sleep(sleeping_time)

    elif(counter == 11):
        socket.send_string("%s %.2f" %("pu10", pu10))
        print("%s %.2f" %("pu10", pu10))
        time.sleep(sleeping_time)

    elif(counter == 12):
        socket.send_string("%s %.2f" %("pu11", pu11))
        print("%s %.2f" %("pu11", pu11))
        time.sleep(sleeping_time)

def scenario_4(i):
    print('scenario 4 {}'.format(i))
    timer = scenario4.iloc[i, 0]
    t1 = scenario4.iloc[i, 1]
    t2 = scenario4.iloc[i, 2]
    t3 = scenario4.iloc[i, 3]
    t4 = scenario4.iloc[i, 4]
    t5 = scenario4.iloc[i, 5]
    t6 = scenario4.iloc[i, 6]
    t7 = scenario4.iloc[i, 7]
    pu01 = scenario4.iloc[i, 10]
    pu02 = scenario4.iloc[i, 11]
    pu03 = scenario4.iloc[i, 12]
    pu10 = truth['FLOW_PU10'].iloc[i]
    pu11 =truth['FLOW_PU11'].iloc[i]

    if(counter == 0):
        socket.send_string("%s %.2f" %("time", timer))
        print("%s %.2f" %("time", timer))
        time.sleep(sleeping_time)
        
    elif(counter == 1):
        socket.send_string("%s %.2f" %("t1", t1))
        print("%s %.2f" %("t1", t1))
        time.sleep(sleeping_time)
        
    elif(counter == 2):
        socket.send_string("%s %.2f" %("t2", t2))
        print("%s %.2f" %("t2", t2))
        time.sleep(sleeping_time)

    elif(counter == 3):
        socket.send_string("%s %.2f" %("t3", t3))
        print("%s %.2f" %("t3", t3))
        time.sleep(sleeping_time)

    elif(counter == 4):
        socket.send_string("%s %.2f" %("t4", t4))
        print("%s %.2f" %("t4", t4))
        time.sleep(sleeping_time)

    elif(counter == 5):
        socket.send_string("%s %.2f" %("t5", t5))
        print("%s %.2f" %("t5", t5))
        time.sleep(sleeping_time)

    elif(counter == 6):
        socket.send_string("%s %.2f" %("t6", t6))
        print("%s %.2f" %("t6", t6))
        time.sleep(sleeping_time)

    elif(counter == 7):
        socket.send_string("%s %.2f" %("t7", t7))
        print("%s %.2f" %("t7", t7))
        time.sleep(sleeping_time)

    elif(counter == 8):
        socket.send_string("%s %.2f" %("pu01", pu01))
        print("%s %.2f" %("pu01", pu01))
        time.sleep(sleeping_time)

    elif(counter == 9):
        socket.send_string("%s %.2f" %("pu02", pu02))
        print("%s %.2f" %("pu02", pu02))
        time.sleep(sleeping_time)

    elif(counter == 10):
        socket.send_string("%s %.2f" %("pu03", pu03))
        print("%s %.2f" %("pu03", pu03))
        time.sleep(sleeping_time)

    elif(counter == 11):
        socket.send_string("%s %.2f" %("pu10", pu10))
        print("%s %.2f" %("pu10", pu10))
        time.sleep(sleeping_time)

    elif(counter == 12):
        socket.send_string("%s %.2f" %("pu11", pu11))
        print("%s %.2f" %("pu11", pu11))
        time.sleep(sleeping_time)

while True:
    message = socket.recv()
    if(message == b"00"):
        if(state != 0):
            state = 0
            timer = 0
        scenario_0(timer)
    elif(message == b"10"):
        if(state != 1):
            state = 1
            timer = 0
        scenario_1(timer)
    elif(message == b"30"):
        if(state != 3):
            state = 3
            timer = 0
        scenario_3(timer)
    elif(message == b"40"):
        if(state != 4):
            state = 4
            timer = 0
        scenario_4(timer)

    # counter is 0 ~ 12
    counter += 1
    if counter == 13:
        counter = 0
        timer += 1
    # timer is 0 ~ 950
    if timer == 950:
        timer = 0
    #socket.send(b"World")
