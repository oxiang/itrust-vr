import pandas as pd
import zmq
import time

#Load dataset
attack_dataset = pd.read_csv('minitown_attack.csv')
#no_attack_dataset = pd.read_csv('minitown_no_attacks.csv')

#NetMQ
context = zmq.Context()
socket = context.socket(zmq.PUB)
socket.bind("tcp://*:5555")

#Send data
timer = 0;

while True:
    print('sending request')
    
    #Retrieve data
    pressure = attack_dataset.iloc[timer, 1]
    pump1 = attack_dataset.iloc[timer, 2]
    pump2 = attack_dataset.iloc[timer, 3]
    
    #Send
    socket.send_string("%s %d" %("time", timer))
    print("%s %d" %("time", timer))
    time.sleep(0.02)
    socket.send_string("%s %d.2" %("node1", pressure))
    print("%s %d.2" %("node1", pressure))
    time.sleep(0.02)
    socket.send_string("%s %d.2" %("pump1", pump1))
    print("%s %d.2" %("pump1", pump1))
    time.sleep(0.02)
    socket.send_string("%s %d.2" %("pump2", pump2))
    print("%s %d.2" %("pump2", pump2))
    
    timer = timer + 1
    
    if timer > 684:
        timer = 0
        
    time.sleep(0.05)
